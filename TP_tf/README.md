# Terraform et autres outils similaires

Terraform permet de décrire un deploiement et l'outil
s'occupe de vérifier sa cohérence et d'appliquer les
changement.

Un plan de déploiement Terraform c'est un répertoire
dans lequel il y a des fichiers quelconques avec 
l'extension `.tf` : c'est à nous de les organiser
rationnellement.

Vous pouvez certain de mes plans dans mon dépôt
GIT : http://framagit.org/jpython/meta

La syntaxe exacte d'un déploiement Terraform est
spécifique à un opérateur de Cloud. Les principes
généraux sont les mêmes.

Problème : Terraform a changé de licence. Il n'est
plus au sens propre libre. Ce n'est pas un pb pour
nous pour l'instant.

Des projets alternatifs ont été lancés à partir de
la dernière version open source de Terraform.

OpenToFu : https://opentofu.org/

Commençons par utiliser le "vrai" Terraform puis
on verra si nos plans sont compatibles avec Opentofu...

1. Télécharger et installer Terraform

(plusieurs méthodes d'installation son possible, je prends
directement le binaire Linux ici

~~~~Bash
$ cd /tmp
$ wget https://releases.hashicorp.com/terraform/1.6.5/terraform_1.6.5_linux_amd64.zip
$ sudo apt install unzip
$ unzip terraform_1.6.5_linux_amd64.zip
$ sudo cp terraform /usr/local/bin
~~~~

On commence par créer un répertoire pour héberger le déploiement
et on y crée le ficher décrivant notre fournisseur de Cloud :

~~~~Bash
$ mkdir Tf
$ cd Tf
~~~~

Fichier `provider.tf` :

~~~~
terraform {
    required_providers {
      scaleway = {
        source = "scaleway/scaleway"
      }
    }
    required_version = ">= 0.13"
  }
~~~~~

On demande à terraform de s'initialiser ce qui va télécharger et
installer le connecteur Scaleway :

~~~~Bash
$ terraform init
~~~~

Ça fonctionne car nous avons installé `scw` et l'avons configuré
(`scw init ...`) dans le TP `TP_scw`, la configuration utilisée
par Terraform est dans `~/.config/scw/config.yaml` tout comme le
client CLI scw.

Il ne reste plus qu'à décrire un premier déploiement avec
une instance pour commencer...

La documentation du connecteur Scaleway pour Terraform est
disponible à :

https://registry.terraform.io/providers/scaleway/scaleway/latest/docs

## Création de ressource : instances

On peut définir une instance ainsi (avec une IP publique) :

~~~~Bash
# Pour vous y retrouver changer "jp" en identifiant
# personnel

resource "scaleway_instance_ip" "jp_srv1" {}

resource "scaleway_instance_server" "jp_srv1" {
    name  = "jp_srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "web", "apache2" ]
    ip_id = scaleway_instance_ip.jp_srv1.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv1.sh")
    }
  }

output "srv1_public_ip" {
  value = "${scaleway_instance_server.jp_srv1.public_ip}"
}
~~~~

À partir d'ici on va utiliser OpenTofu au lieu de Terraform (ils
sont 100% compatible à cette date).

Les commandes de base de Tofu/Terraform sont :

- `tofu init` : installe le connecteur de votre fournisseur de cloud
- `tofu plan` : valide et résume de déploiement
- `tofu apply` : applique le déploiement (en cas de modification/ajout/suppression de ressource le déploiement est modifié
- `tofu output` : montre les ressources déclarées comme telles (typiquement : adresses IP publiques inconnues à l'avance)
- `tofu destroy` : détruit toutes les ressources créées

_N.B._ Le fichier `terraform.tfstate` contient l'état courant de
votre déploiement. Surtout à ne pas mettre dans le dépôt git
du déploiement. Si vous voulez le supprimer pensez à faire
un `tofu destroy` avant !

## À vous ! 

Récupérez le fichiers `*.tf` et scripts de ce répertoire dans un
nouveau répertoire de votre dépôt GIT, changez l'étiquette "jp"
par une qui vous est personnelle.

Vérifiez que le déploiement fonctionne (le script `go` simplifie
la connexion ssh aux instances).

Modifiez le déploiement pour ajouter une instance nommée srv2.

Faites en sorte que apache2 soit installé via cloud-init sur
srv1 et nginx sur srv2. Vérifiez que c'est fait en visitant
avec un nagivateur Web les url `http://ip_de_srv1` et `http://ip_de_srv2`.

