# Pour vous y retrouver changer "jp" en identifiant
# personnel

resource "scaleway_instance_ip" "yosr_srv1" {}

resource "scaleway_instance_server" "yosr_srv1" {
    name  = "yosr_srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "web", "apache2" ]
    ip_id = scaleway_instance_ip.yosr_srv1.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv1.sh")
    }
  }
resource "scaleway_instance_ip" "yosr_srv2" {}

resource "scaleway_instance_server" "yosr_srv2" {
    name  = "yosr_srv2"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "web", "nginx" ]
    ip_id = scaleway_instance_ip.yosr_srv2.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv2.sh")
    }
  }
output "srv1_public_ip" {
  value = "${scaleway_instance_server.yosr_srv1.public_ip}"
}


output "srv2_public_ip" {
  value = "${scaleway_instance_server.yosr_srv2.public_ip}"
}
