#!/usr/bin/env bash
# Install Nginx
apt-get update
apt-get install -y nginx

# Start Nginx
systemctl start nginx
systemctl enable nginx

# Create a file as a part of your original script
touch /tmp/srv2_cloud-init

