#!/usr/bin/env bash

# Install Apache2
apt-get update
apt-get install -y apache2

# Start Apache2
systemctl start apache2
systemctl enable apache2

# Create a file as a part of your original script
touch /tmp/srv1_cloud-init
